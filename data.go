package main

import (
	// "context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	// "time"
	// "go.mongodb.org/mongo-driver/bson"
	// "go.mongodb.org/mongo-driver/mongo"
	// "go.mongodb.org/mongo-driver/mongo/options"
	// "go.mongodb.org/mongo-driver/mongo/readpref"
)

const (
	COMPLETE    string = "complete"
	PENDING     string = "pending"
	IN_PROGRESS string = "in_progress"
)

type StepInfo struct {
	StepID     int    `json:"step_id"'`
	StepName   string `json:"step_name"`
	StepStatus string `json:"step_status"`
}
type JobInfo struct {
	JobID   string
	JobName string
	Steps   []StepInfo
}
type JobsInfo struct {
	Jobs []JobInfo
}

func GetJobInfo() (string, error) {
	log.Println("get job information")
	s1 := StepInfo{
		StepID:     1,
		StepName:   "download",
		StepStatus: COMPLETE,
	}
	s2 := StepInfo{
		StepID:     2,
		StepName:   "replaceBinary" + fmt.Sprint(rand.Int()),
		StepStatus: IN_PROGRESS,
	}
	s3 := StepInfo{
		StepID:     3,
		StepName:   "relink",
		StepStatus: PENDING,
	}
	var jobs []JobInfo
	for i := 0; i < 10000; i++ {
		t := JobInfo{
			JobID:   fmt.Sprint(i),
			JobName: "replaceBinary",
			Steps:   []StepInfo{s1, s2, s3},
		}
		jobs = append(jobs, t)
	}

	js := JobsInfo{Jobs: jobs}
	data, err := json.Marshal(js)
	fmt.Println(string(data))
	return string(data), err
}

// func ConnectMongodb() {
// 	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
// 	defer cancel()
// 	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
// 	if err != nil {
// 		panic(err)
// 	}
// 	if err := client.Ping(context.TODO(), readpref.Primary()); err != nil {
// 		panic(err)
// 	}
// 	usersCollection := client.Database("testing").Collection("users")
// 	user := bson.d{{"fullname", "user 1"}, {"age", 30}}
// 	result, err := userscollection.insertone(context.todo(), user)
// 	if err != nil {
// 		panic(err)
// 	}
// }
