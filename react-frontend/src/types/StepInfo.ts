export type StepInfo = {
  StepID: string;
  step_name: string;
  step_status: string;
};
