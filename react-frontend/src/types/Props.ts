export type Props = {
  rows: Array<any>;
  columns: Array<any>;
  count: number;
  databases: string;
  sql: string;
  setIndexOfFinishedSQL?: (
    update: number | ((oldCount: number) => number)
  ) => void;
  setSqlHistory?: (
    update: Array<any> | ((old: Array<any>) => Array<any>)
  ) => void;
  setSqlBatch?: (
    update: Array<any> | ((old: Array<any>) => Array<any>)
  ) => void;
  sqlBatch?: Array<string>;
  //   setCount: (update: number | ((oldCount: number) => number)) => void;
  setCount: (count: number) => void;
  setSQL: (sql: string) => void;
  setSQLRunning: (sqlRunning: boolean) => void;
  sqlRunning: boolean;
  url: string;
  queryClient: any;
  queryKey: Array<any>;
  modalShowing: boolean;
  setModalShowing: (modalShowing: boolean) => void;
};
