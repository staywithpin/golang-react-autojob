import { StepInfo } from "./StepInfo";

export type Jobs = {
  JobID: string;
  JobName: string;
  Steps: StepInfo[];
};
