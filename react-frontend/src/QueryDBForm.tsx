import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";

import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import SelectDatabases from "./components/SelectDatabases";
import ShowSQLTooltip from "./components/ShowSQLTooltip";
import QueryMTable from "./components/QueryMTable";
import { Props } from "./types/Props";
import HighlightSQL from "./components/HighlightSQL";
import DisplaySQL from "./components/DisplaySql";
import { format } from "sql-formatter";
import MultipleSQL from "./components/MultipleSqls";
import { Accordion } from "react-bootstrap";

const queryClient = new QueryClient();

function QueryDB() {
  const url = window.location.href;
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  // const initSQL: string = ( urlParams.get("sql") ? urlParams.get("sql") : "") as string;
  const initSQL = urlParams.get("sql") || "";
  const [sqlRunning, setSQLRunning] = useState(false);
  const [sqlHistory, setSqlHistory] = useState([]);
  // store sql array when submit several sqls at the same time (input multiple sql in the sql textarea)
  const [sqlBatch, setSqlBatch] = useState([]);
  const [sql, setSQL] = useState(initSQL);

  const [count, setCount] = useState(0);
  // const [data, setData] = useState({ columns: "", rows: "" });
  const [databases, setDatabases] = useState({});

  console.log(url);
  console.log(sql);
  console.log(initSQL);
  const predefined_url = "http://localhost:8000/predefined_sql";
  const queryKey = ["predefined"];

  return (
    <>
      <SelectDatabases setSelectedDatabase={setDatabases} />
      <div>
        <label>
          <li>click the cell will copy the cell value.</li>
          <li>
            If a column name starts as "sql_" (case insensivity), when DOUBLE
            click the cell of the column, the sql in the cell will run
            automatically.
          </li>
        </label>
      </div>

      <Accordion defaultActiveKey={["0"]}>
        <QueryClientProvider client={queryClient}>
          <Accordion.Item eventKey="0">
            <Accordion.Header>pre-defined SQL</Accordion.Header>
            <Accordion.Body>
              <QueryMTable
                setCount={setCount}
                setSQL={setSQL}
                queryKey={queryKey}
                sql={sql}
                setSQLRunning={setSQLRunning}
                url={predefined_url}
                // count={count}
                sqlRunning={sqlRunning}
              />
            </Accordion.Body>
          </Accordion.Item>
          <ReactQueryDevtools initialIsOpen={true} />
        </QueryClientProvider>
      </Accordion>
      <QueryDBForm
        databases={databases}
        setSQL={setSQL}
        sqlRunning={sqlRunning}
        setSQLRunning={setSQLRunning}
        setSqlHistory={setSqlHistory}
        setSqlBatch={setSqlBatch}
        count={count}
        setCount={setCount}
      />

      {console.log(process.env.NODE_ENV)}
      {/* <pre>the current sql:{sql}</pre> */}
      {/* <HighlightSQL sql={sql} /> */}
      <DisplaySQL sql={sql} />

      {/* <ShowSQLTooltip sql={sql} /> */}
      {/* {sql === "" ? (
        ""
      ) : (
        <>
          <QueryClientProvider client={queryClient}>
            <QueryMTable
              setCount={setCount}
              setSQL={setSQL}
              queryKey={[sql, count]}
              sql={sql}
              setSQLRunning={setSQLRunning}
              url=""
              count={count}
              sqlRunning={sqlRunning}
            />
          </QueryClientProvider>
        </>
      )} */}
      <MultipleSQL
        setCount={setCount}
        setSQL={setSQL}
        queryKey={[sql, count]}
        sqlBatch={sqlBatch}
        sql={sql}
        queryClient={queryClient}
        setSQLRunning={setSQLRunning}
        url=""
        count={count}
        sqlRunning={sqlRunning}
      />
    </>
  );
}
function QueryDBForm({
  databases,
  setSqlHistory,
  setSqlBatch,
  setSQL,
  sqlRunning,
  setSQLRunning,
  setCount,
  count,
}: Props) {
  const [text, setText] = useState("");
  const handleText = (e) => {
    var tempSQL: string = e.target.value;
    setText(tempSQL);
    if (tempSQL.includes(";")) {
      tempSQL = format(tempSQL);
      const sqlLines = tempSQL.split(/\r?\n/);
      console.log(sqlLines);
      const result = sqlLines.filter((value: string, index: number) =>
        value.endsWith(";") ? true : false
      );
    }
  };
  const handleSubmit = () => {
    if (text === "") {
      return;
    }
    setSQLRunning(true);
    setCount((cnt)=>(cnt+1));
    console.log(setSqlHistory);
    setSqlHistory((preValue) => {
      console.log("++++++");
      preValue.push(text);
      console.log(preValue);
      return preValue;
    });
    const tempSqlBatch = text.split(";");
    setSqlBatch(tempSqlBatch);
    setSQL(text);
    console.log("run the sql:\n" + text);
    console.log("sql batch is:\n", tempSqlBatch);
  };
  return (
    <>
      <Form>
        <Form.Group className="mb-9" controlId="exampleForm.ControlTextarea1">
          <Form.Label>SQLs to run</Form.Label>
          <Form.Control
            onChange={handleText}
            as="textarea"
            // value={null}
            rows={10}
            disabled={sqlRunning}
          />
        </Form.Group>

        <Button
          variant="primary"
          type="submit"
          disabled={sqlRunning}
          onClick={handleSubmit}
        >
          {sqlRunning ? "Running SQL, please wait a moment..." : "Run Query"}
        </Button>
        <label>{count} SQL executed</label>
      </Form>
    </>
  );
}

export default QueryDB;
