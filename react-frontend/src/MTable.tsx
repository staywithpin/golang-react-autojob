import React, { useMemo, useState } from "react";
import { MaterialReactTable } from "material-react-table";
import ConfirmRunSQL from "./components/ConfirmRunSQL";
import { Props } from "./types/Props";

export default function MTable({
  rows,
  columns,
  setCount,
  setSQL,
  setSQLRunning,
  sqlRunning, // used only a event handler, don't refresh the MTable when it changes
}: Props) {
  const [sqlTemp, setSqlTemp] = useState<string>("");
  const [modalShowing, setModalShowing] = useState(false);
  const [url, setUrl] = useState("");
  var rows_transformed = rows.map(function (e, _) {
    return Object.fromEntries(
      new Map(
        e.map(function (value: any, index: number) {
          return [
            columns[index]?.["name"],
            /* TODO NOTE react JSX can't handle boolean well */
            value === true || value === false ? value.toString() : value,
          ];
        })
      )
    );
  });
  const [data, setData] = React.useState(rows_transformed);
  const columns_def = useMemo(
    () =>
      columns.map((val, _: number) => {
        return {
          accessorKey: val["name"],
          header: val["name"],
          id: val["name"],
        };
      }),
    []
  );

  console.log(columns);

  return (
    <>
      {/* <ConfirmRunSQL
          sql={sqlTemp}
          url={url}
          setSQL={setSQL}
          modalShowing={modalShowing}
          setSQLRunning={setSQLRunning}
          setCount={setCount}
          setModalShowing={setModalShowing}
          /> */}
      <MaterialReactTable
        enableClickToCopy
        enableColumnFilterModes
        initialState={{
          density: "compact",
          showColumnFilters: true,
          pagination: { pageSize: 25, pageIndex: 0 },
        }}
        positionGlobalFilter="left"
        enablePagination={true}
        positionPagination="both"
        enableRowNumbers
        onHoveredColumnChange={(e) => {
          alert("test");
        }}
        muiTableProps={{
          sx: {
            tableLayout: "fixed",
          },
        }}
        muiTableBodyProps={{
          sx: {
            //stripe the rows, make odd rows a darker color
            "& tr:nth-of-type(odd)": {
              backgroundColor: "#f5f5f5",
            },
          },
        }}
        positionToolbarAlertBanner="bottom"
        muiTableBodyCellProps={({ cell }) => ({
          onDoubleClick: () => {
            const columnName = cell.column.id;
            console.log("............");
            console.log(columnName);

            if (
              columnName.toLowerCase().startsWith("sql_") ||
              columnName.toLowerCase() === "sql"
            ) {
              console.log("=============");
              if (sqlRunning) {
                console.log("sql is running, ignore...");
              } else {
                console.log("sql is not running, need to run the new sql");
                console.log(columnName);
                const sql = cell.getValue();
                console.log("sql is:" + sql);
                setSqlTemp(sql);
                const currentUrl = window.location.href;
                const queryStr = "?db=database1&sql=" + sql;
                const url = encodeURI(currentUrl + queryStr);
                setUrl(url);
                setModalShowing(true);
              }
            }
          },
        })}
        columns={columns_def}
        data={data}
      />
    </>
  );
}
