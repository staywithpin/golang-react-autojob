import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { format } from "sql-formatter";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { dark } from "react-syntax-highlighter/dist/esm/styles/prism";
import { Props } from "../types/Props";

const ConfirmRunSQL = ({
  sql = "select * from dual",
  modalShowing,
  setModalShowing,
  setSQLRunning,
  setSQL,
  setCount,
  url,
}: Props) => {
  sql = format(sql);
  console.log(sql);
  return (
    <div
      className="modal show"
      style={{ display: "block", position: "initial" }}
    >
      <Modal show={modalShowing} size="xl">
        {/* <Modal.Dialog size="xl"> */}
        <Modal.Header>
          <Modal.Title>Run the following SQL</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <SyntaxHighlighter language="sql" style={dark}>
            {sql}
          </SyntaxHighlighter>
        </Modal.Body>

        <Modal.Footer>
          <Button
            variant="primary"
            onClick={() => {
              console.log("sql is:" + sql);
              setSQL(sql);
              setSQLRunning(true);
              setCount((count) => count + 1);
              setModalShowing(false);
            }}
          >
            Run in a the current tab
          </Button>
          <Button
            variant="secondary"
            onClick={() => {
              window.open(url, "new tab");
              setModalShowing(false);
            }}
          >
            Run in a new tab
          </Button>
          <Button
            variant="secondary"
            onClick={() => {
              setModalShowing(false);
            }}
          >
            Cancel
          </Button>
        </Modal.Footer>
        {/* </Modal.Dialog> */}
      </Modal>
    </div>
  );
};

export default ConfirmRunSQL;
