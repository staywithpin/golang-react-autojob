import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { dark } from "react-syntax-highlighter/dist/esm/styles/prism";
import { format } from "sql-formatter";
import "./ShowSQLTooltip.css";

interface Props {
  sql: string;
}
const HighlightSQL = ({ sql }: Props) => {
  sql = format(sql);
  return (
    <SyntaxHighlighter language="sql" style={dark}>
      {sql}
    </SyntaxHighlighter>
  );
};

export default HighlightSQL;
