import Button from "react-bootstrap/Button";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { dark } from "react-syntax-highlighter/dist/esm/styles/prism";
import { format } from "sql-formatter";
import "./ShowSQLTooltip.css";

const ShowSQLTooltip = ({ sql }) => {
  sql = format(sql);
  const tooltipStyle = {
    color: "blue",
    width: "1000px",
  };
  const renderTooltip = (props) => (
    <Tooltip
      id="button-tooltip"
      style={tooltipStyle}
      placement="auto-start"
      {...props}
    >
      <SyntaxHighlighter language="sql" style={dark}>
        {sql}
      </SyntaxHighlighter>
    </Tooltip>
  );

  return (
    <OverlayTrigger
      placement="right"
      delay={{ show: 250, hide: 400 }}
      overlay={renderTooltip}
    >
      <Button variant="success">Hover me to see formated SQL</Button>
    </OverlayTrigger>
  );
};

export default ShowSQLTooltip;
