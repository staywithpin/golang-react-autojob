import Accordion from "react-bootstrap/Accordion";
import { QueryClientProvider } from "@tanstack/react-query";
import { Props } from "../types/Props";
import QueryMTable from "./QueryMTable";
import { useEffect, useState } from "react";

function MultipleSQL({
  sqlRunning,
  setSQLRunning,
  count,
  queryClient,
  sqlBatch,
  queryKey,
  setSQL,
  setCount,
}: Props) {
  // const sql = "select * from system_tables";
  const [indexOfFinishedSQL, setIndexOfFinishedSQL] = useState(0);
  useEffect(() => {
    console.log("completed", sqlBatch, indexOfFinishedSQL, count);
    if (indexOfFinishedSQL == sqlBatch.length) {
      console.log("completed", count, indexOfFinishedSQL, sqlBatch.length);
      setSQLRunning(false);
      setIndexOfFinishedSQL(0);
      /* setCount((cnt) => cnt + 1); */
    }
    //   if (indexOfFinishedSQL >= sqlBatch.length ) {
    //     console.log("completed , ....", indexOfFinishedSQL);
    //     setIndexOfFinishedSQL(-1);
    //   }
  }, [indexOfFinishedSQL]);
  // useEffect(() => {
  //   if (indexOfFinishedSQL >= sqlBatch.length ) {
  //     console.log("completed , ....", indexOfFinishedSQL);
  //     setIndexOfFinishedSQL(-1);
  //   }
  // }, [indexOfFinishedSQL]);

  if (sqlBatch?.length == 1) {
    const value = sqlBatch[0];
    return (
      <>
        <pre>current index is {indexOfFinishedSQL}</pre>
        <pre>current count is {count}</pre>
        <QueryClientProvider client={queryClient}>
          <QueryMTable
            setCount={setCount}
            setSQL={setSQL}
            queryKey={[value, count]}
            sql={value}
            setIndexOfFinishedSQL={setIndexOfFinishedSQL}
            setSQLRunning={setSQLRunning}
            url=""
            count={count}
            sqlRunning={sqlRunning}
          />
        </QueryClientProvider>
      </>
    );
  } else {
    return (
      <>
        <pre>current index is {indexOfFinishedSQL}</pre>
        <pre>current count is {count}</pre>
        <Accordion defaultActiveKey={["0"]} alwaysOpen>
          {sqlBatch.map((value, index) => {
            console.log("completed ", value, index, indexOfFinishedSQL);
            if (index <= indexOfFinishedSQL || !sqlRunning) {
              console.log("begin to run sql " + value);
              return (
                <Accordion.Item eventKey={index.toString()} key={index}>
                  <Accordion.Header>
                    SQL {index + 1}:{value.substring(0, 100)}
                  </Accordion.Header>
                  <Accordion.Body>
                    <QueryClientProvider client={queryClient}>
                      <QueryMTable
                        setCount={setCount}
                        setSQL={setSQL}
                        queryKey={[value, count]}
                        sql={value}
                        setIndexOfFinishedSQL={setIndexOfFinishedSQL}
                        setSQLRunning={setSQLRunning}
                        url=""
                        count={count}
                        sqlRunning={sqlRunning}
                      />
                    </QueryClientProvider>
                  </Accordion.Body>
                </Accordion.Item>
              );
            }
          })}
        </Accordion>
      </>
    );
  }
}

export default MultipleSQL;
