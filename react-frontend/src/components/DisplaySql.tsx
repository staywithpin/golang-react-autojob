import { useState } from "react";
import Form from "react-bootstrap/Form";
import HighlightSQL from "./HighlightSQL";
function DisplaySQL({ sql }) {
  const rawSQL = "Show raw SQL";
  const highlightedSQL = "Show highlighted formated SQL";
  const noShow = "Don't show SQL";
  const [selectedText, setSelectedText] = useState(noShow);
  const clickHander = (e) => {
    const currentValue = e.target.value;
    setSelectedText(currentValue);
  };
  console.log(selectedText);
  return (
    <>
      <Form.Select size="lg" onChange={clickHander}>
        <option>{noShow}</option>
        <option>{highlightedSQL}</option>
        <option>{rawSQL}</option>
      </Form.Select>
      {selectedText === rawSQL ? <pre>{sql}</pre> : true}
      {selectedText === highlightedSQL ? <HighlightSQL sql={sql} /> : true}
    </>
  );
}
export default DisplaySQL;
