import { useState } from "react";
import Button from "react-bootstrap/Button";
const initCount = 0;
export default function TestComp() {
  const [count, setCount] = useState(0);
  var bbb = "test";
  return (
    <>
      <Button
        variant="primary"
        onClick={() => {
          setCount((x) => x + 1);
          console.log(count);
        }}
      >
        refresh the other button
      </Button>
      {bbb === "test" ? <MyBotton cnt={count}></MyBotton> : <pre></pre>}
    </>
  );
}

function MyBotton({ cnt }) {
  const [count, setCount] = useState(initCount);
  return (
    <>
      <Button
        variant="primary"
        onClick={() => {
          setCount((x) => x + 1);
          console.log(count);
        }}
      >
        {cnt}
      </Button>
    </>
  );
}
