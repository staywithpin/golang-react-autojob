import Form from "react-bootstrap/Form";

export default function SelectDatabases({ setSelectedDatabase }) {
  const type = "checkbox";
  const onClickHander = (e) => {
    const label = e.target.labels[0].textContent;
    const enabled = e.target.checked;
    setSelectedDatabase((x) => {
      x[label] = enabled;
      console.log(x);
      return x;
    });
  };
  return (
    <Form>
      <div key={`inline-${type}`} className="mb-3">
        <Form.Check
          inline
          label="east01"
          name="group1"
          defaultChecked={true}
          type={type}
          onClick={onClickHander}
          id={`inline-${type}-1`}
        />
        <Form.Check
          inline
          label="west01"
          name="group1"
          type={type}
          onClick={onClickHander}
          id={`inline-${type}-2`}
        />
      </div>
    </Form>
  );
}
