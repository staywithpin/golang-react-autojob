import { ColumnDef, createColumnHelper } from "@tanstack/react-table";
import { Jobs } from "../types/Jobs";
import JobStepsDetails from "../JobStepsDetail";

const columnHelper = createColumnHelper<Jobs>();
export const jobColumnDef: ColumnDef<Jobs, any>[] = [
  columnHelper.accessor("JobName", {
    header: () => <span>Job Name</span>,
    cell: (info) => info.getValue(),
    footer: (info) => info.column.id,
  }),
  columnHelper.accessor("JobID", {
    header: () => <span>Job ID</span>,
    cell: (info) => info.getValue(),
    footer: (info) => info.column.id,
  }),
  columnHelper.accessor("Steps", {
    header: () => <span>Job Details</span>,
    cell: (props) => <JobStepsDetails jobSteps={props.getValue()} />,
    footer: (info) => info.column.id,
  }),
];
