import MTable from "../MTable";
import "bootstrap/dist/css/bootstrap.min.css";
import { useEffect } from "react";
import { Props } from "../types/Props";
import { useQuery } from "@tanstack/react-query";
// interface Props {
//   url: string;
//   sqlRunning: boolean;
//   setSQLRunning: (sqlRunning: boolean) => void;
//   count: number;
//   sql: string;
//   queryKey: Array<any>;
//   setSQL: (sql: string) => void;
//   setCount: (count: number) => void;
// }
function QueryMTable({
  url,
  sqlRunning,
  setSQLRunning,
  setIndexOfFinishedSQL,
  sql,
  queryKey,
  setSQL,
  setCount,
}: Props) {
  if (url === "") {
    url = "http://localhost:8000" + "/dbquery?sql=" + sql;
  }

  url = encodeURI(url);
  console.log(url);
  const { isLoading, error, isFetching, isFetched, data, isSuccess, status } =
    useQuery({
      queryKey: queryKey, // use count, so that each new sql run will trigger a fetch, otherwise it will fetch from cache.
      cacheTime: 1000 * 3600 * 24 * 365 * 10,
      staleTime: 1000 * 3600 * 24 * 365 * 10,
      refetchOnReconnect: false,
      queryFn: () =>
        // fetch("http://localhost:8000").then((res) => res.json()),
        fetch(url).then((res) => res.json()),
    });

  console.log(isSuccess, sqlRunning, status);
  /* console.log(data);
   * console.log("run sql to db:" + sql); */
  //   useEffect(() => {
  //     console.log(
  //       "use effct:",
  //       isSuccess,
  //       sqlRunning,
  //       status,
  //       isFetched,
  //       isFetching
  //     );
  //     if (status === "success" || isFetched) {
  //       setSQLRunning(false);
  //       /* console.log("set the button to true"); */
  //     }
  //   }, [status, isFetched, isFetching]);

  useEffect(() => {
    console.log(
      "/////////////",
      status,
      setIndexOfFinishedSQL ? "defined" : "undefined"
    );
    if (setIndexOfFinishedSQL && status === "success") {
      setIndexOfFinishedSQL((old) => {
        console.log("set index to ", old + 1, sql);
        return old + 1;
      });
    }
  }, [status]);

  if (isFetching) {
    console.log("is fetching...");
  }

  if (isFetched) {
    console.log("is fetched...");
  }

  if (isLoading) {
    console.log("is loading...");
    return <pre>Loading...</pre>;
  }

  if (error) {
    return <pre> "An error has occurred: " + error.message</pre>;
  }

  console.log("reay to paint table");
  // if (setIndexOfFinishedSQL) {
  //   setIndexOfFinishedSQL((index) => {
  //     console.log("index is " + index);
  //     return index + 1;
  //   });
  // }
  // the following code is wrong, as it is actually conditionally run which is not supported.
  // useEffect(() => {
  //   console.log("use effct:", isSuccess, sqlRunning, status);
  //   if (status === "success" && sqlRunning) {
  //     setSQLRunning(true);
  //     console.log("set the button to true");
  //   }
  // }, [status]);

  return (
    <MTable
      setCount={setCount}
      setSQL={setSQL}
      columns={data["columns_list"]}
      rows={data["data"]}
      setSQLRunning={setSQLRunning}
      sqlRunning={sqlRunning}
    ></MTable>
  );
}

export default QueryMTable;
