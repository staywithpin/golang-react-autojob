import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import QueryDB from "./QueryDBForm.tsx";
import TestComp from "./components/Test.tsx";

const columns: Array<object> = [
  { name: "col1" },
  { name: "col2" },
  { name: "col3" },
  { name: "col4" },
];

const rows = [
  [1, "test2", "test4", "test"],
  [1, 2, "test1", 4],
  [1, "est2", 3, 4],
  [1, 2, 3, 4],
  // { col1: 2, col2: 2, col3: "test" },
  // { col1: 2, col2: 2, col3: "test2" },
  // { col1: 3, col2: 2 },
  // { col1: 2, col2: 2 },
  // { col1: 1, col2: 2 },
  // { col1: 2, col2: 2 },
  // { col1: 2, col2: 2 },
  // { col1: 2, col2: 2 },
  // { col1: 2, col2: 2 },
  // { col1: 2, col2: 2 },
  // { col1: 2, col2: 2 },
  // { col1: 2, col2: 2 },
  // { col1: 2, col2: 2 },
  // { col1: 2, col2: 2 },
  // { col1: 2, col2: 2 },
  // { col1: 2, col2: 2 },
  // { col1: 2, col2: 2 },
  // { col1: 2, col2: 2 },
  // { col1: 2, col2: 2 },
];

const databases = [
  {
    type: "vertica",
    name: "east1",
  },
  {
    type: "vertica",
    name: "east2",
  },
];
ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  // <React.StrictMode>
  <>
    <QueryDB />
    {/* <TestComp /> */}
  </>
  // </React.StrictMode>
);
