import "bootstrap/dist/css/bootstrap.min.css";
import React, { useEffect, useMemo, useState } from "react";
import Pagination from "./components/Pagination";
import { jobColumnDef } from "./components/JobColumnDefs";
import {
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  PaginationState,
  getFilteredRowModel,
  getPaginationRowModel,
  useReactTable,
} from "@tanstack/react-table";
import {
  QueryClient,
  QueryClientProvider,
  useQuery,
} from "@tanstack/react-query";

// https://medium.com/tenjin-engineering/using-react-query-with-react-table-884158535424

const url = "/api";
const queryClient = new QueryClient();

function Jobs() {
  const {
    isLoading,
    isFetching,
    isError,
    data: jobs,
    error,
    isSuccess,
  } = useQuery({
    queryKey: ["jobs"],
    queryFn: () => fetch(url).then((res) => res.json()),
    refetchInterval: 100000,
    staleTime: 20000,
  });

  const [data, setData] = React.useState([]);
  const [{ pageIndex, pageSize }, setPagination] = useState<PaginationState>({
    pageIndex: 0,
    pageSize: 10,
  });

  useEffect(() => {
    if (isSuccess) {
      setData(jobs["Jobs"]);
      console.log(jobs);
    }
  }, [jobs]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetch(url);
        const json = await res.json();
        setData(json["Jobs"]);
      } catch (error) {
        console.log(error);
      }
    };
    const id = setInterval(() => {
      fetchData();
    }, 1000);
    return () => clearInterval(id);
  });

  const pagination = useMemo(
    () => ({
      pageIndex,
      pageSize,
    }),
    [pageIndex, pageSize]
  );

  const table = useReactTable({
    data,
    columns: jobColumnDef,
    pageCount: data?.pageCount,
    state: {
      pagination,
    },
    autoResetPageIndex: false, // without setting it to false, every time data are refreshed, the index will be reset to 0
    getPaginationRowModel: getPaginationRowModel(),
    onPaginationChange: setPagination,
    getCoreRowModel: getCoreRowModel(),
    defaultColumn: {
      size: 0, // set default size to fit-content
    },
  });

  if (isLoading) return "Loading....";
  if (isFetching) return "Fetching....";

  if (isError) return "An error has occured:" + error;
  return (
    <>
      <table className="table table-zebra my-4 w-full">
        <thead>
          {table.getHeaderGroups().map((headerGroup) => (
            <tr key={headerGroup.id}>
              {headerGroup.headers.map((header) => (
                <th key={header.id}>
                  {header.isPlaceholder
                    ? null
                    : flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody>
          {table.getRowModel().rows.map((row) => (
            <tr key={row.id}>
              {row.getVisibleCells().map((cell) => (
                <td key={cell.id}>
                  {flexRender(cell.column.columnDef.cell, cell.getContext())}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
        <tfoot>
          {table.getFooterGroups().map((footerGroup) => (
            <tr key={footerGroup.id}>
              {footerGroup.headers.map((header) => (
                <th key={header.id}>aaa</th>
              ))}
            </tr>
          ))}
        </tfoot>
      </table>
      <Pagination table={table} />
    </>
  );
}

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Jobs />
    </QueryClientProvider>
  );
}

export default App;
