import Dropdown from 'react-bootstrap/Dropdown';
import {Table, ProgressBar} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

function JobStepsDetails( {jobSteps}) {
    return (
        <Table>
            <tbody>
                {jobSteps.map((jobStep, index) => {
                    const step_status= jobStep["step_status"]
                    const step_name = jobStep["step_name"]
                    const aninated = step_status=== "in_progress";

                    let variant:string;
                    let progressBar;
                    switch (step_status) {
                        case "in_progress":
                            variant = "info";
                            break;
                        case "complete":
                            variant = "success";
                            break;
                        case "pending":
                            variant = "warning";
                            break
                        default:
                            variant = "danger";
                            break;
                    }
                    const step_name_to_show = index + 1 + ":" + step_name;
                    progressBar = <ProgressBar now='100' variant={variant} label= {step_name_to_show } animated={aninated}/>
                    if (step_status==="pending"){
                        progressBar = step_name_to_show
                    }
                    return (
                    <tr key={jobStep["step_id"]}>
                        <td>
                            {progressBar}
                        </td>
                        <td>{jobStep["step_status"]}</td>
                        <td>
                            <Dropdown>
                                <Dropdown.Toggle variant="success" id="dropdown-basic">
                                    Action
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                    <Dropdown.Item onClick={() => window.open(jobStep["step_url"], "_blank")}>
                                        View
                                    </Dropdown.Item>
                                    <Dropdown.Item onClick={() => window.open(jobStep["step_url"], "_blank")}>
                                        Stop
                                    </Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>

                        </td>
                        <td>
                            <button className="btn btn-primary" onClick={() => {
                                window.location.href = `/job-steps/${jobStep.id}`
                            }}>View log</button>
                        </td>
                    </tr>
                    );
                })
            }
            
            </tbody>
        </Table>

    )
}

export default JobStepsDetails
