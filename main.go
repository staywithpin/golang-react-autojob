package main

import (
	"embed"
	"fmt"
	"io/fs"
	"net/http"
	"os"
	"time"

	cache "github.com/victorspringer/http-cache"
	"github.com/victorspringer/http-cache/adapter/memory"
)

const (
	statics string = "/static/"
	dir     string = "react-frontend/dist"
)

var (
	//go:embed react-frontend/dist
	web embed.FS
)

func fileHandle(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "simple test")
}

func apiHandle(w http.ResponseWriter, r *http.Request) {
	// json.NewEncoder(w).Encode("test")

	data, _ := GetJobInfo()
	fmt.Fprint(w, string(data))
}

func main() {
	// ConnectMongodb()
	fmt.Println(statics)
	memcached, err := memory.NewAdapter(
		memory.AdapterWithAlgorithm(memory.LRU),
		memory.AdapterWithCapacity(10000000),
	)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	cacheClient, err := cache.NewClient(
		cache.ClientWithAdapter(memcached),
		cache.ClientWithTTL(3*time.Second),
		cache.ClientWithRefreshKey("opn"),
	)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	dist, err := fs.Sub(web, dir)
	if err != nil {
		panic(err)
	}
	http.Handle("/", http.FileServer(http.FS(dist)))
	http.Handle("/api", cacheClient.Middleware(http.HandlerFunc(apiHandle)))
	err = http.ListenAndServe(":3333", nil)
	if err != nil {
		panic(err)
	}
}
